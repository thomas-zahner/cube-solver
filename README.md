# Cube solver

## Background info

This is the source code of my final school project done in 2017.

The idea of this project is to program a [LEGO Mindstorms EV3](https://en.wikipedia.org/wiki/Lego_Mindstorms_EV3) to solve the Rubik's cube.
This has already been done by the [MindCuber](https://www.mindcuber.com/) project.
In fact the only difference to the MindCuber project is the software implementation because the MindCuber building instructions were used to assemble the Mindstorms for this project.

Using the [LeJos](http://www.lejos.org/) framework and Eclipse with the LeJos plugin the Mindstorms could be programmed in Java.

There is a method for such a task called the [two-phase-algorithm](http://kociemba.org/cube.htm).
This algorithms efficiency is optimal and it shouldn't produce any solutions longer than twenty moves.
But at the same time the two-phase-algorithm is really complex and I wasn't even able to understand it.

This is why I decided to use the method for cube solving I already knew: the [CFOP method](http://www.ws.binghamton.edu/fridrich/system.html)
I even simplified the F2L step to the beginner method because I the implementation turned out to be not that easy.
But it turned out to be a good decision after all because I could really focus on the code implementation.
If I had used the two-phase-algorithm I probably wouldn't have been able to implement my own ideas to such an extent.

## Results

The colour sensor the Mindstorms is using turned out to be rather finicky and inaccurate.
Because of that I added a small program to calibrate the colour sensor and the possibility to fix the scan result manually if it's incorrect.

Ten tests have been made to compare the average performance between the MindCuber and the cube solver:

|             | Scan time (s) | Total time (s) | Moves | Time per move (s) |
|-------------|---------------|----------------|-------|-------------------|
| MindCuber   |            45 |            108 |    23 |               2.8 |
| Cube solver |            60 |            521 |    95 |               4.9 |

### Last thoughts

These bare results are pretty depressing.
At the same time I wasn't expecting my program to do much better mainly because I was using a simplified CFOP method.

Also the fact that sometimes we have to correct the scan results manually is a bit annoying.
Nonetheless I was happy with my project because no matter how you scramble the Rubik's cube it can be solved by the cube solver.

Also once the scan contains no errors the cube solver will apply the calculated solution reliably to the Rubik's cube.
I can say I have learned tons of new stuff and had fun experimenting all at the same time.

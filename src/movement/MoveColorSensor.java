package movement;

import lejos.hardware.motor.Motor;
import lejos.utility.Delay;

public class MoveColorSensor
{
	private static boolean overCube = false;
	private static boolean overEdge = true;
	private static boolean isSpecial = false;

	/**
	 * Move the color sensor into the starting position.
	 */
	public static void reset()
	{
		Motor.C.setSpeed(400);
		Motor.C.forward();
		while (!Motor.C.isStalled())
		{
		}
		Motor.C.stop();
		Motor.C.backward();
		Delay.msDelay(1150);
		Motor.C.stop();
	}

	/**
	 * Move the color sensor over the cube (always edge position) if it's in
	 * the starting position, otherwise it will move the color sensor back to
	 * the starting position.
	 */
	public static void move()
	{
		if (overCube)
		{
			if (overEdge)
			{
				Motor.C.setSpeed(400);
				Motor.C.forward();
				Delay.msDelay(440);
				Motor.C.stop();
				overCube = false;
			}
			else
			{
				moveOverEdge(false);
				move();
			}
		}
		else
		{
			Motor.C.setSpeed(400);
			Motor.C.backward();
			Delay.msDelay(440);
			Motor.C.stop();
			overCube = true;
		}
	}

	public static void moveOverEdge(boolean special)
	{
		if (overEdge) return;
		isSpecial = special;
		Motor.C.setSpeed(400);
		Motor.C.backward();
		if (!isSpecial) Delay.msDelay(190);
		else Delay.msDelay(220);
		Motor.C.stop();
		overEdge = true;
	}

	public static void moveOverCorner()
	{
		if (!overEdge) return;
		Motor.C.setSpeed(400);
		Motor.C.forward();
		if (!isSpecial) Delay.msDelay(190);
		else
		{
			Delay.msDelay(220);
			isSpecial = false;
		}
		Motor.C.stop();
		overEdge = false;
	}
}

package util;

import java.util.ArrayList;

public class SplitSolution
{
	public static ArrayList splitSolution(String solution)
	{
		ArrayList moves = new ArrayList();
		char apostrophe = 39; // '
		int i = 0;

		while (i < solution.length())
		{
			int moveLength = 1;

			if (i + 1 <= solution.length() - 1)
			{
				if (solution.charAt(i + 1) == '2')
				{
					moveLength = 2;
				}
				else if (solution.charAt(i + 1) == apostrophe)
				{
					moveLength = 2;
				}
			}

			moves.add(solution.substring(i, i + moveLength));
			i += moveLength;
		}

		return moves;
	}
}

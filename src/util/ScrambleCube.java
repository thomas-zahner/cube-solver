package util;

import java.util.Random;

public class ScrambleCube
{
	public static String scramble(Cube cube)
	{
		Random random = new Random();
		String currentRandomMove = "";
		String oldRandomMove = "x";
		String scramble = "";

		for (int i = 0; i < 17; i++)
		{
			switch (random.nextInt(12))
			{
			case 0:
				currentRandomMove = "U";
				break;
			case 1:
				currentRandomMove = "U'";
				break;
			case 2:
				currentRandomMove = "D";
				break;
			case 3:
				currentRandomMove = "D'";
				break;
			case 4:
				currentRandomMove = "L";
				break;
			case 5:
				currentRandomMove = "L'";
				break;
			case 6:
				currentRandomMove = "R";
				break;
			case 7:
				currentRandomMove = "R'";
				break;
			case 8:
				currentRandomMove = "F";
				break;
			case 9:
				currentRandomMove = "F'";
				break;
			case 10:
				currentRandomMove = "B";
				break;
			case 11:
				currentRandomMove = "B'";
				break;
			}

			//Make repetition of the same layers twice in a row impossible
			if (currentRandomMove.charAt(0) != oldRandomMove.charAt(0))
			{
				//Apply move
				cube.applyRotation(currentRandomMove);
				scramble += currentRandomMove;

				oldRandomMove = currentRandomMove;
			}
			else i--;
		}

		return scramble;
	}
}

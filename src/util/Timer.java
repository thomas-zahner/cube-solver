package util;

public class Timer
{
	private static long start;

	/**
	 * Start the timer
	 */
	public static void start()
	{
		start = System.currentTimeMillis();
	}

	/**
	 * @return Passed time since start() was called in milliseconds
	 */
	public static long getPassedTime()
	{
		return System.currentTimeMillis() - start;
	}
}
